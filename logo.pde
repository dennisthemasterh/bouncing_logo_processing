
PImage img;
import java.util.Map;

int imgX=102; 
int imgY=61;
float posX = random(0, 360-imgX);
float posY = random(0, 720-imgY);//minus img so no going over border
float chgX=5; 
float chgY=5;
float r = random(100, 150);
float g = random(100, 150);
float b = random(100, 150);

void setup() {
  size(1280, 720);//16:9 resolution
  img = loadImage("dvd1.png");  //determine imgX
  //readConfig();
  img.resize(imgX, imgY);
}

void draw() {
  background(0);
  fill(r, g, b, 255);
  rect(posX, posY, imgX, imgY);
  image(img, posX, posY);

  posX-=chgX;
  posY-=chgY;

  if (posX<=0 || posX>=1280-imgX) {
    chgX*=-1;
    chgColor();
  }
  if (posY<=0 || posY>=720-imgY) {
    chgY*=-1;
    chgColor();
  }
}

void chgColor() {
  int[][] extreme = {{255, 0, 0}, {255, 128, 0}, {155, 155, 0}, {102, 204, 0}, {0, 255, 0}, {0, 255, 128}, {0, 244, 244}, {0, 128, 255}, {0, 0, 255}, {127, 0, 255}, {255, 0, 255}, {255, 0, 128}};
  int[] c = extreme[int(random(0, extreme.length))];
  r = c[0];
  g = c[1];
  b = c[2];
}

void readConfig() {      
  String[] lines = loadStrings("setting.cfg");
  for (int i = 0; i < lines.length; i++) {
    String[] pair=lines[i].split("=");
    switch(pair[0]) {
    case "horizontal speed": 
      chgX=float(pair[1]);
      break;
    case "vertical speed": 
      chgY=float(pair[1]);
      break;
    case "width":
      imgX=int(pair[1]);
      break;
    case "height": 
      imgY=int(pair[1]);
      break;
    }
  }
}
